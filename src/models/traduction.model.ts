export interface traductionModel {
	path: string;
	isJoker: boolean;
	traduction: string;
	jokerString?: string;
}
