export interface Lang {
	label: string;
	pathFile: string;
	isJoker: boolean;
	jokerString?: string;
}
