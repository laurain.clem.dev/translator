import { InitService } from '../services/init.service';
import cli from 'cli-ux';
import * as inquirer from 'inquirer';
// @ts-ignore
import { PathPrompt } from 'inquirer-path';
import { prompt } from 'inquirer';
import * as figlet from 'figlet';
import { Command } from '@oclif/command';
import { MessageService } from '../services/message.service';
import { type } from 'os';
import { TradService } from '../services/trad.service';
import { Lang } from '../models/lang.model';

const tradDirectoryPathQuestion = [
	{
		type: 'path',
		name: 'traductionDirectoryPath',
		message: 'Traduction files directory :',
		default: process.cwd(),
	},
];

export default class Init extends Command {
	static description = 'Initialize the basics configuration.';
	initService: InitService = new InitService(this);
	messageService = new MessageService();

	async run() {
		console.log(figlet.textSync('Translator'));
		if (this.initService.checkIfConfAlreadyExists()) {
			const continueInit = await inquirer
				.prompt([
					{
						type: 'confirm',
						name: 'continueInit',
						message:
							'A configuration already exist, do you want to overide it ?',
					},
				])
				.then((r) => {
					return r.continueInit;
				});
			if (continueInit) {
				this.initialization();
			} else {
				this.exit();
			}
		} else {
			this.initialization();
		}
	}

	async initialization(): Promise<void> {
		inquirer.registerPrompt('path', PathPrompt);

		let traductionDirectoryPath = await inquirer
			.prompt(tradDirectoryPathQuestion)
			.then((r) => {
				return r.traductionDirectoryPath;
			});
		const choices: { checked: boolean; name: string }[] = [];
		this.initService
			.getTraductionFilesList(traductionDirectoryPath)
			.forEach((file) => {
				choices.push({ checked: true, name: file });
			});
		if (choices.length > 0) {
			inquirer
				.prompt({
					type: 'checkbox',
					name: 'filesCheckbox',
					message: 'Select files you wanted to be managed',
					choices: choices,
				})
				.then(async (res) => {
					this.initService.configFile = [];

					for (let i = 0; i < res.filesCheckbox.length; i++) {
						const label = await cli.prompt(
							`Label for [${res.filesCheckbox[i]}]`,
						);

						this.initService.configFile.push({
							label: label,
							pathFile: `${traductionDirectoryPath}/${res.filesCheckbox[i]}`,
							isJoker: false,
						});
					}

					this.initService.initialize();

					inquirer
						.prompt({
							name: 'isJokerLang',
							message: 'Do you want to use joker langs',
							type: 'confirm',
						})
						.then((r) => {
							const langsAvailable: string[] = [];
							this.initService.getConfig().forEach((lang) => {
								langsAvailable.push(lang.label);
							});

							if (r.isJokerLang) {
								inquirer
									.prompt({
										name: 'jokerLangsList',
										message: 'Which one ?',
										type: 'checkbox',
										choices: langsAvailable,
									})
									.then(async (res) => {
										const jokerString = await inquirer
											.prompt({
												type: 'input',
												name: 'jokerString',
												message: 'Which joker traduction do you want',
											})
											.then((r) => {
												return r.jokerString;
											});
										this.initService.addJokerLangs(
											res.jokerLangsList,
											jokerString,
										);
									});
							} else {
							}
						});
				});
		} else {
			console.log(
				this.messageService.errorMsg(),
				"This file isn't containing json files",
			);
		}
	}
}
