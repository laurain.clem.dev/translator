import { Command, flags } from '@oclif/command';
import { TradService } from '../../services/trad.service';
import cli from 'cli-ux';
import { traductionModel } from '../../models/traduction.model';
import { Lang } from '../../models/lang.model';

export default class TradAdd extends Command {
	static description = 'Add a new traduction';

	static args = [
		{ name: 'traductionKey', required: true, description: 'traduction key' },
	];

	tradService = new TradService(this);

	async run() {
		const { args } = this.parse(TradAdd);
		const langs = this.tradService.getLangs();
		const langsFiltered: Lang[] = [];
		const jokerLangs: Lang[] = [];
		langs.forEach((lang) => {
			if (!lang.isJoker) {
				langsFiltered.push(lang);
			} else {
				jokerLangs.push(lang);
			}
		});
		const traductions: traductionModel[] = [];
		for (let i = 0; i < langsFiltered.length; i++) {
			traductions.push({
				path: langsFiltered[i].pathFile,
				traduction: await cli.prompt(`${langsFiltered[i].label}`),
				isJoker: langsFiltered[i].isJoker,
				jokerString: langsFiltered[i].jokerString,
			});
		}
		for (let i = 0; i < jokerLangs.length; i++) {
			traductions.push({
				path: jokerLangs[i].pathFile,
				//@ts-ignore
				traduction: jokerLangs[i].jokerString,
				isJoker: jokerLangs[i].isJoker,
				jokerString: jokerLangs[i].jokerString,
			});
		}
		this.tradService.add(args.traductionKey, traductions);
	}
}
