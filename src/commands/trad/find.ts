import { Command, flags } from '@oclif/command';
import { TradService } from '../../services/trad.service';

export default class TradFind extends Command {
	static description = 'Find a traduction';

	static args = [
		{ name: 'traductionKey', required: true, description: 'traduction key' },
	];

	tradService = new TradService(this);

	async run() {
		const { args } = this.parse(TradFind);
		this.tradService.find(args.traductionKey);
	}
}
