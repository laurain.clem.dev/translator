import { Command, flags } from '@oclif/command';
import { TradService } from '../../services/trad.service';

export default class TradRemove extends Command {
	static description = 'Remove a traduction';

	static args = [
		{ name: 'traductionKey', required: true, description: 'traduction key' },
	];

	tradService = new TradService(this);

	async run() {
		const { args } = this.parse(TradRemove);

		this.tradService.remove(args.traductionKey);
	}
}
