import { Lang } from '../models/lang.model';
import * as fs from 'fs';
import { homedir } from 'os';
import { MessageService } from './message.service';
import { traductionModel } from '../models/traduction.model';
import { encode } from 'punycode';
import { exec } from 'child_process';

const pathConfigFile = `${homedir()}/translator/config.json`;
const encodeType = 'utf-8';

export class TradService {
	configFile: Lang[] = [];
	command: any;
	messageService = new MessageService();

	constructor(command: any) {
		this.command = command;
		if (fs.existsSync(pathConfigFile)) {
			this.configFile = JSON.parse(
				fs.readFileSync(pathConfigFile, {
					encoding: encodeType,
				}),
			);
		} else {
			console.log(
				this.messageService.errorMsg(),
				'You have to initialize me with the command "translator init"',
			);
			this.command.exit();
		}
		if (this.configFile === undefined) {
			console.log(
				this.messageService.errorMsg(),
				'You have to initialize me with the command "translator init"',
			);
			this.command.exit();
		}
	}

	add(traductionKey: string, traductions: traductionModel[]) {
		const tradPaths: Lang[] = [];
		traductions.forEach((traduction) => {
			tradPaths.push({
				label: '',
				pathFile: traduction.path,
				isJoker: traduction.isJoker,
				jokerString: traduction.jokerString,
			});
		});
		this.checkIfAllFilesExist(tradPaths);
		traductions.forEach((traduction) => {
			let currentTraductions = JSON.parse(
				fs.readFileSync(traduction.path, {
					encoding: encodeType,
				}),
			);
			/* if (!traduction.isJoker) { */
			currentTraductions[traductionKey] = traduction.traduction;
			fs.writeFileSync(traduction.path, JSON.stringify(currentTraductions), {
				encoding: encodeType,
			});
			/* } else {
				currentTraductions[traductionKey] = traduction.jokerString;
			} */

			exec(`prettier --write ${traduction.path}`, (error, stdout, stderr) => {
				if (error) {
					console.log(`error: ${error.message}`);
					return;
				}
				if (stderr) {
					console.log(`stderr: ${stderr}`);
				}
			});
		});
		console.log(this.messageService.doneMsg(), '✨ All traductions done !');
	}

	remove(traductionKey: string) {
		const langs: Lang[] = this.getLangs();
		this.checkIfAllFilesExist(langs);

		langs.forEach((lang) => {
			const currentTraductions = JSON.parse(
				fs.readFileSync(lang.pathFile, {
					encoding: encodeType,
				}),
			);
			if (currentTraductions[traductionKey] !== undefined) {
				delete currentTraductions[traductionKey];
				fs.writeFileSync(lang.pathFile, JSON.stringify(currentTraductions), {
					encoding: encodeType,
				});
				exec(`prettier --write ${lang.pathFile}`, (error, stdout, stderr) => {
					if (error) {
						console.log(`error: ${error.message}`);
						return;
					}
					if (stderr) {
						console.log(`stderr: ${stderr}`);
					}
				});
				console.log(this.messageService.doneMsg(), lang.label);
			} else {
				console.log(this.messageService.warnMsg(), 'Traduction not found 😿');
			}
		});
	}

	find(traductionKey: string) {
		const langs: Lang[] = this.getLangs();
		this.checkIfAllFilesExist(langs);

		langs.forEach((lang) => {
			const currentTraductions = JSON.parse(
				fs.readFileSync(lang.pathFile, {
					encoding: encodeType,
				}),
			);
			if (currentTraductions[traductionKey]) {
				console.log(lang.label, ': ', currentTraductions[traductionKey]);
			} else {
				console.log(this.messageService.warnMsg(), 'Traduction not found 😿');
			}
		});
	}

	getLangs(): Lang[] {
		if (fs.existsSync(pathConfigFile)) {
			return JSON.parse(
				fs.readFileSync(pathConfigFile, { encoding: encodeType }),
			);
		} else {
			console.log(
				this.messageService.errorMsg(),
				'You have to initialize me with the command "translator init"',
			);
			this.command.exit();
			// @ts-ignore
			return;
		}
	}

	checkIfAllFilesExist(traductions: Lang[]) {
		let isGood = true;
		traductions.forEach((traduction) => {
			if (isGood) {
				const fileExist = fs.existsSync(traduction.pathFile);
				if (!fileExist) {
					console.log(
						this.messageService.errorMsg(),
						`[${traduction.pathFile}] \n no longer exits`,
					);
					this.command.exit();
				}
			}
		});
	}
}
