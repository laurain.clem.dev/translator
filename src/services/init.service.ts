import * as fs from 'fs';
import { homedir } from 'os';
import { Command } from '@oclif/command';
import { Lang } from '../models/lang.model';
import { MessageService } from './message.service';
import { config } from 'process';
import { encode } from 'punycode';
import { exec } from 'child_process';

const pathConfigFile = `${homedir()}/translator/config.json`;
const encodeType = 'utf-8';

export class InitService {
	configFile: Lang[] = [];
	command: any;
	messageService = new MessageService();

	constructor(command: any) {
		this.command = command;

		if (fs.existsSync(pathConfigFile)) {
			this.configFile = JSON.parse(
				fs.readFileSync(pathConfigFile, {
					encoding: encodeType,
				}),
			);
		} else {
			if (!fs.existsSync(`${homedir()}/translator`)) {
				fs.mkdirSync(`${homedir()}/translator`);
			}
			fs.writeFileSync(pathConfigFile, '[]', { encoding: encodeType });
		}
		if (this.configFile === undefined) {
			console.log(
				this.messageService.errorMsg(),
				'An error occured while initializing configuration file.',
			);
			this.command.exit();
		}
	}

	initialize(): void {
		if (fs.existsSync(pathConfigFile)) {
			fs.writeFileSync(pathConfigFile, JSON.stringify(this.configFile), {
				encoding: encodeType,
			});
			console.log('✨🎉 Traducer is correcly initialized with :');
			for (let i = 0; i < this.configFile.length; i++) {
				console.log(i + 1, ' ', this.configFile[i].label);
			}
		} else {
			console.log(this.messageService.errorMsg(), 'Configuration failed.');
		}
	}

	getTraductionFilesList(path: string): string[] {
		return (fs.readdirSync(path, {
			encoding: encodeType,
		}) as string[]).filter((file) => file.includes('.json'));
	}

	checkIfConfAlreadyExists() {
		return this.configFile.length > 0;
	}

	getConfig() {
		return this.configFile;
	}

	addJokerLangs(jokerLangs: string[], jokerString: string) {
		const currentConfig: Array<Lang> = JSON.parse(
			fs.readFileSync(pathConfigFile, {
				encoding: encodeType,
			}),
		);
		currentConfig.forEach((lang) => {
			if (jokerLangs.includes(lang.label)) {
				lang.jokerString = jokerString;
				lang.isJoker = true;
			}
		});
		fs.writeFileSync(pathConfigFile, JSON.stringify(currentConfig), {
			encoding: encodeType,
		});
		exec(`prettier --write ${pathConfigFile}`, (error, stdout, stderr) => {
			if (error) {
				console.log(`error: ${error.message}`);
				return;
			}
			if (stderr) {
				console.log(`stderr: ${stderr}`);
			}
		});
		console.log('✨🎉 All done !');
	}
}
