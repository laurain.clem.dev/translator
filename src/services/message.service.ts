import * as chalk from 'chalk';

export class MessageService {
	errorMsg(): string {
		return chalk.bold.bgHex('#e74c3c')(' ERROR ') + ' ';
	}

	doneMsg(): string {
		return chalk.bold.bgHex('#2ecc71')(' DONE ') + ' ';
	}

	warnMsg(): string {
		return chalk.bold.bgHex('#f1c40f')(' WARN ') + ' ';
	}
}
