translator
==========

A little tool to manage translations with json files and ngx-translate.

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/translator.svg)](https://npmjs.org/package/translator)
[![Downloads/week](https://img.shields.io/npm/dw/translator.svg)](https://npmjs.org/package/translator)
[![License](https://img.shields.io/npm/l/translator.svg)](https://github.com/Dev/translator/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @vocaza/tradeasy
$ translator COMMAND
running command...
$ translator (-v|--version|version)
@vocaza/tradeasy/2.0.0 win32-x64 node-v12.13.0
$ translator --help [COMMAND]
USAGE
  $ translator COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`translator help [COMMAND]`](#translator-help-command)
* [`translator init`](#translator-init)
* [`translator trad:add TRADUCTIONKEY`](#translator-tradadd-traductionkey)
* [`translator trad:find TRADUCTIONKEY`](#translator-tradfind-traductionkey)
* [`translator trad:remove TRADUCTIONKEY`](#translator-tradremove-traductionkey)

## `translator help [COMMAND]`

display help for translator

```
USAGE
  $ translator help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.1.0/src\commands\help.ts)_

## `translator init`

Initialize the basics configuration.

```
USAGE
  $ translator init
```

_See code: [src\commands\init.ts](https://github.com/Dev/translator/blob/v2.0.0/src\commands\init.ts)_

## `translator trad:add TRADUCTIONKEY`

Add a new traduction

```
USAGE
  $ translator trad:add TRADUCTIONKEY

ARGUMENTS
  TRADUCTIONKEY  traduction key
```

_See code: [src\commands\trad\add.ts](https://github.com/Dev/translator/blob/v2.0.0/src\commands\trad\add.ts)_

## `translator trad:find TRADUCTIONKEY`

Find a traduction

```
USAGE
  $ translator trad:find TRADUCTIONKEY

ARGUMENTS
  TRADUCTIONKEY  traduction key
```

_See code: [src\commands\trad\find.ts](https://github.com/Dev/translator/blob/v2.0.0/src\commands\trad\find.ts)_

## `translator trad:remove TRADUCTIONKEY`

Remove a traduction

```
USAGE
  $ translator trad:remove TRADUCTIONKEY

ARGUMENTS
  TRADUCTIONKEY  traduction key
```

_See code: [src\commands\trad\remove.ts](https://github.com/Dev/translator/blob/v2.0.0/src\commands\trad\remove.ts)_
<!-- commandsstop -->
